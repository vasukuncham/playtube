
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'pdp/:code', 'name': 'pdp', component: () => import('pages/details.vue') },
      { path: 'search', 'name': 'search', component: () => import('pages/search.vue') },
      { path: '/trending', 'name': 'trending', component: () => import('pages/trending.vue') },
      { path: '/history', 'name': 'history', component: () => import('pages/history.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
