export default {
  // Dashboard state
  dash_loader: false,
  dash_video_data: [],
  selected_video_data: [],
  related_videos: [],
  search_results: []
}
