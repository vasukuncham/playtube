export function updateLoader (state, value) {
  state.dash_loader = value
}

export function updateVideoData (state, videoData) {
  if (videoData.items.length > 0) {
    let finalData = []
    videoData.items.forEach((item, index) => {
      let privacy = item.status.privacyStatus
      if (privacy === 'public') {
        let video = {
          id: item.id,
          channelTitle: item.snippet.channelTitle,
          title: item.snippet.title,
          desc: item.snippet.description,
          publishedAt: item.snippet.publishedAt,
          thumbnail: item.snippet.thumbnails.medium.url,
          commentsCount: item.statistics.commentCount,
          dislikeCount: item.statistics.dislikeCount,
          favoriteCount: item.statistics.favoriteCount,
          likeCount: item.statistics.likeCount,
          viewCount: item.statistics.viewCount
        }
        finalData.push(video)
      }
    })
    state.dash_video_data = finalData
  }
}

export function updateSelectedVideoData (state, videoData) {
  if (videoData.items.length > 0) {
    videoData.items.forEach((item, index) => {
      let video = {
        id: item.id,
        channelTitle: item.snippet.channelTitle,
        title: item.snippet.title,
        desc: item.snippet.description,
        publishedAt: item.snippet.publishedAt,
        thumbnail: item.snippet.thumbnails.medium.url,
        commentsCount: item.statistics.commentCount,
        dislikeCount: item.statistics.dislikeCount,
        favoriteCount: item.statistics.favoriteCount,
        likeCount: item.statistics.likeCount,
        viewCount: item.statistics.viewCount
      }
      state.selected_video_data = video
    })
  }
}

export function updateRelatedVideoData (state, videoData) {
  if (videoData.items.length > 0) {
    let finalData = []
    videoData.items.forEach((item, index) => {
      let video = {
        id: item.id,
        channelTitle: item.snippet.channelTitle,
        title: item.snippet.title,
        desc: item.snippet.description,
        publishedAt: item.snippet.publishedAt,
        thumbnail: item.snippet.thumbnails.medium.url
      }
      finalData.push(video)
    })
    state.related_videos = finalData
  }
}

export function updateSearchVideoData (state, videoData) {
  if (videoData.items.length > 0) {
    let finalData = []
    videoData.items.forEach((item, index) => {
      if (item.snippet.hasOwnProperty('thumbnails')) {
        let video = {
          id: item.id,
          channelTitle: item.snippet.channelTitle,
          title: item.snippet.title,
          desc: item.snippet.description,
          publishedAt: item.snippet.publishedAt,
          thumbnail: item.snippet.thumbnails.medium.url
        }
        finalData.push(video)
      }
    })
    state.search_results = finalData
  }
}
