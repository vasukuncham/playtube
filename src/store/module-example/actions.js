import axios from 'axios'

let API_KEY = 'AIzaSyDXY5dT0OWLIqmuMR8EpK8XNr-iVN3iGE0'

export function getVideoData (context) {
  context.commit('updateLoader', true)

  let url = `https://www.googleapis.com/youtube/v3/videos` // ?key=${API_KEY}&part=snippet,contentDetails,statistics,status`
  let params = {
    part: 'snippet,statistics,status',
    chart: 'mostPopular',
    kind: 'youtube#videoListResponse',
    maxResults: 50,
    regionCode: 'IN',
    key: API_KEY
  }
  axios.get(url, { params: params })
    .then((response) => {
      if (response.status === 200) {
        context.commit('updateVideoData', response.data)
        context.commit('updateLoader', false)
      }
    }).catch((error) => {
      console.log(error)
    })
}

export function getSelectedVideoData (context, id) {
  context.commit('updateLoader', true)

  let url = `https://www.googleapis.com/youtube/v3/videos` // ?key=${API_KEY}&part=snippet,contentDetails,statistics,status`
  let params = {
    id: id,
    part: 'snippet,statistics',
    key: API_KEY
  }
  axios.get(url, { params: params })
    .then((response) => {
      if (response.status === 200) {
        context.commit('updateSelectedVideoData', response.data)
        context.commit('updateLoader', false)
      }
    }).catch((error) => {
      console.log(error)
    })
}

export function getRelatedVideoData (context, id) {
  context.commit('updateLoader', true)

  let url = `https://www.googleapis.com/youtube/v3/search` // ?key=${API_KEY}&part=snippet,contentDetails,statistics,status`
  let params = {
    relatedToVideoId: id,
    type: 'video',
    part: 'snippet',
    maxResults: 30,
    regionCode: 'IN',
    key: API_KEY
  }
  axios.get(url, { params: params })
    .then((response) => {
      if (response.status === 200) {
        context.commit('updateRelatedVideoData', response.data)
        context.commit('updateLoader', false)
      }
    }).catch((error) => {
      console.log(error)
    })
}

export function getSearchDetails (context, term) {
  context.commit('updateLoader', true)

  let url = `https://www.googleapis.com/youtube/v3/search` // ?key=${API_KEY}&part=snippet,contentDetails,statistics,status`
  let params = {
    q: term,
    part: 'snippet',
    maxResults: 25,
    key: API_KEY
  }
  axios.get(url, { params: params })
    .then((response) => {
      if (response.status === 200) {
        context.commit('updateSearchVideoData', response.data)
        context.commit('updateLoader', false)
      }
    }).catch((error) => {
      console.log(error)
    })
}
