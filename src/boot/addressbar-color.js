// import something here
import { AddressbarColor } from 'quasar'

// "async" is optional
/* export default async ({  app, router, Vue, ...  }) => {} */
export default () => {
  AddressbarColor.set('#ffffff')
}
